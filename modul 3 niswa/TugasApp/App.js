import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Home from './Tugas12/index'
import LoginScreen from './Tugas13/login'
import SignUp from './Tugas13/signup'
import About from './Tugas13/about'
import Aplikasi from './Tugas14/aplikasi'
import Tugas15 from './Tugas15'
import Tugas15_2 from './Tugas15/2/Index'


export default function App() {
  return (
    //<Home/>
    //<LoginScreen/>
    //<SignUp/>
    //<About/>
    //<Aplikasi/>
    <Tugas15/>
    //<Tugas15_2/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
